<?xml version="1.0" ?> 

<!-- 
	Stylesheet for Microsoft Word 2010:
	Bibliography formatting according to the Bitex standard.
     
	Author(s):	Karsten Jahn (karsten@k-jahn.de)
			Jaime Chavarriaga (jaime.chavarriaga@gmail.com)
               
	Copyright:	Copyright (c) 2012 Karsten Jahn

                Permission is hereby granted, free of charge, to any person obtaining a
                copy of this software and associated documentation files (the "Software"),
                to deal in the Software without restriction, including without limitation
                the rights to use, copy, modify, merge, publish, distribute, sublicense,
                and/or sell copies of the Software, and to permit persons to whom the
                Software is furnished to do so, subject to the following conditions:

                The above copyright notice and this permission notice shall be included 
                in all copies or substantial portions of the Software.

                THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
                IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
                FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
                THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
                LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
                FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
                DEALINGS IN THE SOFTWARE.
	
	ToDo:	Escape special Characters for Latex.
		Add Source types...
		- Misc				(misc)
		- Thesis 			(No direct Bibtex type! Master or PhD?)
		- Performance			(No direct Bibtex type!)
		- SoundRecording		(No direct Bibtex type!)
		- Art				(No direct Bibtex type!)
		- DocumentFromInternetSite	(No direct Bibtex type!)
		- InternetSite			(No direct Bibtex type!)
		- Film				(No direct Bibtex type!)
		- Interview			(No direct Bibtex type!)
		- Patent			(No direct Bibtex type!)
		- ElectronicSource		(No direct Bibtex type!)
		- Case				(No direct Bibtex type!)
					
	Version Overview:
		1.1	Localized Stylename to support Office 2016
		1.0	Simplified Important Fields section, limited to 3 fields now.
			New types added:
			- ArticleInAPeriodical		(article)
			- BookSection			(inbook)
			- ConferenceProceedings		(inproceedings)
			- JournalArticle		(article)
			- Report			(techreport)				
		0.1	Set up
			@book type
			"Person" formatting, incl. no return if empty
			Address creation and formatting
  -->

<!-- List of the external resources that we are referencing --> 

<!--xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:b="http://schemas.openxmlformats.org/officeDocument/2006/bibliography"-->
<xsl:stylesheet version="1.0" xmlns:b="http://schemas.openxmlformats.org/officeDocument/2006/bibliography" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">

<!-- When the bibliography or citation is in your document, it's just HTML --> 

<xsl:output method="html" encoding="us-ascii"/> 

<!-- match the root element, and dispatch to its children --> 

<xsl:template match="/"> 
	<xsl:apply-templates select="*" /> 
</xsl:template> 

<!-- initial input --> 
<xsl:template match="b:version"> 
    <xsl:text>1.0</xsl:text> 
</xsl:template>
<xsl:template match="b:URL"> 
    <!-- Omit URL in the output -->
    <!--
    <xsl:text>http://www.k-jahn.de/files/bibtex.xsl</xsl:text> 
      -->
</xsl:template>
<xsl:template match="b:author"> 
    <xsl:text>Karsten Jahn (karsten@k-jahn.de)</xsl:text> 
</xsl:template>
<xsl:template match="b:description"> 
    <xsl:text>Formatting the Word Bibliography to a Bibtex compatible style.</xsl:text> 
</xsl:template>
<xsl:template match="b:StyleName"> 
    <xsl:text>Bibtex Export</xsl:text> 
</xsl:template>
<xsl:template match="b:StyleNameLocalized">
  <xsl:text>Bibtex Export</xsl:text> 
</xsl:template>

<!-- 
	The GetImportantFields specify which fields should appear in the 
	Create Source dialog (when in a collapsed state) in Word.
	This should be done for every SourceType used.
	
	For simplicity, this field has not taken care of. This style's
	purpose is the export of an already created library. Therefore 
	all SourceTypes only ask for the Author, Title and Year in this 
	definition.
 --> 
<xsl:template match="b:GetImportantFields[b:SourceType = 'Book']">
  <b:ImportantFields>
    <b:ImportantField>
        <xsl:text>b:Author/b:Author/b:NameList</xsl:text>
    </b:ImportantField>
    <b:ImportantField>
        <xsl:text>b:Title</xsl:text>
    </b:ImportantField>
    <b:ImportantField>
        <xsl:text>b:Year</xsl:text>
    </b:ImportantField>
  </b:ImportantFields>
</xsl:template>

<xsl:template match="b:GetImportantFields[b:SourceType = 'Report']">
  <b:ImportantFields>
	<b:ImportantField>
		<xsl:text>b:Author/b:Author/b:NameList</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Title</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Year</xsl:text>
	</b:ImportantField>
  </b:ImportantFields>
</xsl:template>

<xsl:template match="b:GetImportantFields[b:SourceType = 'ArticleInAPeriodical']">
  <b:ImportantFields>
	<b:ImportantField>
		<xsl:text>b:Author/b:Author/b:NameList</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Title</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Year</xsl:text>
	</b:ImportantField>
  </b:ImportantFields>
</xsl:template>

<xsl:template match="b:GetImportantFields[b:SourceType = 'JournalArticle']">
  <b:ImportantFields>
	<b:ImportantField>
		<xsl:text>b:Author/b:Author/b:NameList</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Title</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Year</xsl:text>
	</b:ImportantField>
  </b:ImportantFields>
</xsl:template>

<xsl:template match="b:GetImportantFields[b:SourceType = 'ConferenceProceedings']">
  <b:ImportantFields>
	<b:ImportantField>
		<xsl:text>b:Author/b:Author/b:NameList</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Title</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Year</xsl:text>
	</b:ImportantField>
  </b:ImportantFields>
</xsl:template>

<xsl:template match="b:GetImportantFields[b:SourceType = 'BookSection']">
  <b:ImportantFields>
	<b:ImportantField>
		<xsl:text>b:Author/b:Author/b:NameList</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Title</xsl:text>
	</b:ImportantField>
	<b:ImportantField>
		<xsl:text>b:Year</xsl:text>
	</b:ImportantField>
  </b:ImportantFields>
</xsl:template>



<!-- 
	The Source specifies the layout of the bibliography in Word
	for every SourceType. To prepare a bibliothecal file according
	to the Bibtex standard, every single field is printed, not 
	only the important ones. If a field is empty, it will not 
	appear. The Tag field is takes as the ID. 
	This has to be done for every SourceType used.
--> 

<!-- Word(Book) to Bibtex(book) -->
<xsl:template match="b:Source[b:SourceType = 'Book']">
  <xsl:variable name="address">
	<xsl:call-template name="format-address">
		<xsl:with-param name="city" select="b:City"></xsl:with-param>
		<xsl:with-param name="state" select="b:StateProvince"></xsl:with-param>
		<xsl:with-param name="country" select="b:CountryRegion"></xsl:with-param>
	</xsl:call-template>
  </xsl:variable>

  <p>
    <xsl:text>@book{</xsl:text>
	<xsl:value-of select="b:Tag"/>
	<xsl:if test="b:Author/b:Author/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>author = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Author/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="$address != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>address = {</xsl:text>
		<xsl:value-of select="$address"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Title != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>title = {</xsl:text>
		<xsl:value-of select="b:Title"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Year != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>year = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Month != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>month = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Publisher != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>publisher = {</xsl:text>
		<xsl:value-of select="b:Publisher"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Volume != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>volume = {</xsl:text>
		<xsl:value-of select="b:Volume"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Edition != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>edition = {</xsl:text>
		<xsl:value-of select="b:Edition"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Pages != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>pages = {</xsl:text>
		<xsl:value-of select="b:Pages"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Author/b:Editor/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>editor = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Editor/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:StandardNumber != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>isbn = {</xsl:text>
		<xsl:value-of select="b:StandardNumber"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
    <br />
	<xsl:text>}</xsl:text>
  </p>
</xsl:template>

<!-- Word(Report) to Bibtex(techreport) -->
<xsl:template match="b:Source[b:SourceType = 'Report']">
  <xsl:variable name="address">
	<xsl:call-template name="format-address">
		<xsl:with-param name="city" select="b:City"></xsl:with-param>
		<xsl:with-param name="state" select="b:StateProvince"></xsl:with-param>
		<xsl:with-param name="country" select="b:CountryRegion"></xsl:with-param>
	</xsl:call-template>
  </xsl:variable>

  <p>
    <xsl:text>@techreport{</xsl:text>
	<xsl:value-of select="b:Tag"/>
	<xsl:if test="b:Author/b:Author/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>author = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Author/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="$address != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>address = {</xsl:text>
		<xsl:value-of select="$address"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Title != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>title = {</xsl:text>
		<xsl:value-of select="b:Title"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Year != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>year = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Month != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>month = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Publisher != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>publisher = {</xsl:text>
		<xsl:value-of select="b:Publisher"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Volume != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>volume = {</xsl:text>
		<xsl:value-of select="b:Volume"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Edition != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>edition = {</xsl:text>
		<xsl:value-of select="b:Edition"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Pages != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>pages = {</xsl:text>
		<xsl:value-of select="b:Pages"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Author/b:Editor/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>editor = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Editor/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:StandardNumber != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>isbn = {</xsl:text>
		<xsl:value-of select="b:StandardNumber"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
    <br />
	<xsl:text>}</xsl:text>
  </p>
</xsl:template>

<!-- Word(ArticleInAPeriodical) to Bibtex(article) -->
<xsl:template match="b:Source[b:SourceType = 'ArticleInAPeriodical']">
  <xsl:variable name="address">
	<xsl:call-template name="format-address">
		<xsl:with-param name="city" select="b:City"></xsl:with-param>
		<xsl:with-param name="state" select="b:StateProvince"></xsl:with-param>
		<xsl:with-param name="country" select="b:CountryRegion"></xsl:with-param>
	</xsl:call-template>
  </xsl:variable>

  <p>
    <xsl:text>@article{</xsl:text>
	<xsl:value-of select="b:Tag"/>
	<xsl:if test="b:Author/b:Author/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>author = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Author/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="$address != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>address = {</xsl:text>
		<xsl:value-of select="$address"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Title != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>title = {</xsl:text>
		<xsl:value-of select="b:Title"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:PeriodicalTitle != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>journal = {</xsl:text>
		<xsl:value-of select="b:PeriodicalTitle"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Year != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>year = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Month != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>month = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Publisher != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>publisher = {</xsl:text>
		<xsl:value-of select="b:Publisher"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Volume != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>volume = {</xsl:text>
		<xsl:value-of select="b:Volume"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Edition != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>edition = {</xsl:text>
		<xsl:value-of select="b:Edition"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Pages != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>pages = {</xsl:text>
		<xsl:value-of select="b:Pages"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Author/b:Editor/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>editor = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Editor/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:StandardNumber != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>isbn = {</xsl:text>
		<xsl:value-of select="b:StandardNumber"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
    <br />
	<xsl:text>}</xsl:text>
  </p>
</xsl:template>

<!-- Word(JournalArticle) to Bibtex(article) -->
<xsl:template match="b:Source[b:SourceType = 'JournalArticle']">
  <xsl:variable name="address">
	<xsl:call-template name="format-address">
		<xsl:with-param name="city" select="b:City"></xsl:with-param>
		<xsl:with-param name="state" select="b:StateProvince"></xsl:with-param>
		<xsl:with-param name="country" select="b:CountryRegion"></xsl:with-param>
	</xsl:call-template>
  </xsl:variable>

  <p>
    <xsl:text>@article{</xsl:text>
	<xsl:value-of select="b:Tag"/>
	<xsl:if test="b:Author/b:Author/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>author = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Author/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="$address != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>address = {</xsl:text>
		<xsl:value-of select="$address"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Title != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>title = {</xsl:text>
		<xsl:value-of select="b:Title"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:JournalName != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>journal = {</xsl:text>
		<xsl:value-of select="b:JournalName"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Year != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>year = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Month != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>month = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Publisher != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>publisher = {</xsl:text>
		<xsl:value-of select="b:Publisher"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Volume != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>volume = {</xsl:text>
		<xsl:value-of select="b:Volume"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Edition != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>edition = {</xsl:text>
		<xsl:value-of select="b:Edition"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Pages != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>pages = {</xsl:text>
		<xsl:value-of select="b:Pages"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Author/b:Editor/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>editor = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Editor/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:StandardNumber != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>isbn = {</xsl:text>
		<xsl:value-of select="b:StandardNumber"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
    <br />
	<xsl:text>}</xsl:text>
  </p>
</xsl:template>

<!-- Word(ConferenceProceedings) to Bibtex(inproceedings) -->
<xsl:template match="b:Source[b:SourceType = 'ConferenceProceedings']">
  <xsl:variable name="address">
	<xsl:call-template name="format-address">
		<xsl:with-param name="city" select="b:City"></xsl:with-param>
		<xsl:with-param name="state" select="b:StateProvince"></xsl:with-param>
		<xsl:with-param name="country" select="b:CountryRegion"></xsl:with-param>
	</xsl:call-template>
  </xsl:variable>

  <p>
    <xsl:text>@inproceedings{</xsl:text>
	<xsl:value-of select="b:Tag"/>
	<xsl:if test="b:Author/b:Author/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>author = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Author/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="$address != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>address = {</xsl:text>
		<xsl:value-of select="$address"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Title != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>title = {</xsl:text>
		<xsl:value-of select="b:Title"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:JournalName != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>booktitle = {</xsl:text>
		<xsl:value-of select="b:JournalName"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Year != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>year = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Month != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>month = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Publisher != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>publisher = {</xsl:text>
		<xsl:value-of select="b:Publisher"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Volume != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>volume = {</xsl:text>
		<xsl:value-of select="b:Volume"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Edition != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>edition = {</xsl:text>
		<xsl:value-of select="b:Edition"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Pages != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>pages = {</xsl:text>
		<xsl:value-of select="b:Pages"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Author/b:Editor/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>editor = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Editor/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:StandardNumber != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>isbn = {</xsl:text>
		<xsl:value-of select="b:StandardNumber"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
    <br />
	<xsl:text>}</xsl:text>
  </p>
</xsl:template>

<!-- Word(BookSection) to Bibtex(inbook) -->
<xsl:template match="b:Source[b:SourceType = 'BookSection']">
  <xsl:variable name="address">
	<xsl:call-template name="format-address">
		<xsl:with-param name="city" select="b:City"></xsl:with-param>
		<xsl:with-param name="state" select="b:StateProvince"></xsl:with-param>
		<xsl:with-param name="country" select="b:CountryRegion"></xsl:with-param>
	</xsl:call-template>
  </xsl:variable>

  <p>
    <xsl:text>@inbook{</xsl:text>
	<xsl:value-of select="b:Tag"/>
	<xsl:if test="b:Author/b:Author/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>author = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:Author/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="$address != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>address = {</xsl:text>
		<xsl:value-of select="$address"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Title != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>title = {</xsl:text>
		<xsl:value-of select="b:Title"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:BookTitle != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>booktitle = {</xsl:text>
		<xsl:value-of select="b:BookTitle"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Year != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>year = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Month != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>month = {</xsl:text>
		<xsl:value-of select="b:Year"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Publisher != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>publisher = {</xsl:text>
		<xsl:value-of select="b:Publisher"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Volume != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>volume = {</xsl:text>
		<xsl:value-of select="b:Volume"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Edition != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>edition = {</xsl:text>
		<xsl:value-of select="b:Edition"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Pages != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>pages = {</xsl:text>
		<xsl:value-of select="b:Pages"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:Author/b:BookAuthor/b:NameList != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>editor = {</xsl:text>
		<xsl:call-template name="format-person-list">
		  <xsl:with-param name="person-list" select="b:Author/b:BookAuthor/b:NameList"></xsl:with-param>
		</xsl:call-template>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="b:StandardNumber != ''">
		<xsl:text>,</xsl:text><br />
		<xsl:text>isbn = {</xsl:text>
		<xsl:value-of select="b:StandardNumber"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
    <br />
	<xsl:text>}</xsl:text>
  </p>
</xsl:template>



<!-- Defines the output of the entire Bibliography --> 
<xsl:template match="b:Bibliography"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
        <xsl:apply-templates select ="*"> 
        </xsl:apply-templates> 
    </body> 
  </html> 
</xsl:template> 



<!-- 
	Defines the output of the Citation.
	Has to be done for every defined SourceType.
 --> 
<xsl:template match="b:Citation/b:Source[b:SourceType = 'Book']"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
		<!-- Defines the output format as (Author, Year)--> 
        <xsl:text>(</xsl:text> 
		<xsl:value-of select="b:Tag"/>
        <xsl:text>)</xsl:text> 
    </body> 
  </html> 
</xsl:template>

<xsl:template match="b:Citation/b:Source[b:SourceType = 'Report']"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
		<!-- Defines the output format as (Author, Year)--> 
        <xsl:text>(</xsl:text> 
		<xsl:value-of select="b:Tag"/>
        <xsl:text>)</xsl:text> 
    </body> 
  </html> 
</xsl:template>

<xsl:template match="b:Citation/b:Source[b:SourceType = 'ArticleInAPeriodical']"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
		<!-- Defines the output format as (Author, Year)--> 
        <xsl:text>(</xsl:text> 
		<xsl:value-of select="b:Tag"/>
        <xsl:text>)</xsl:text> 
    </body> 
  </html> 
</xsl:template>

<xsl:template match="b:Citation/b:Source[b:SourceType = 'JournalArticle']"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
		<!-- Defines the output format as (Author, Year)--> 
        <xsl:text>(</xsl:text> 
		<xsl:value-of select="b:Tag"/>
        <xsl:text>)</xsl:text> 
    </body> 
  </html> 
</xsl:template>

<xsl:template match="b:Citation/b:Source[b:SourceType = 'ConferenceProceedings']"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
		<!-- Defines the output format as (Author, Year)--> 
        <xsl:text>(</xsl:text> 
		<xsl:value-of select="b:Tag"/>
        <xsl:text>)</xsl:text> 
    </body> 
  </html> 
</xsl:template>

<xsl:template match="b:Citation/b:Source[b:SourceType = 'BookSection']"> 
  <html xmlns="http://www.w3.org/TR/REC-html40"> 
    <body> 
		<!-- Defines the output format as (Author, Year)--> 
        <xsl:text>(</xsl:text> 
		<xsl:value-of select="b:Tag"/>
        <xsl:text>)</xsl:text> 
    </body> 
  </html> 
</xsl:template>


<!--
	Functions to be called from above...
-->

<!-- function to format the address fields -->
<xsl:template name="format-address">
	<xsl:param name="city"></xsl:param>
	<xsl:param name="state"></xsl:param>
	<xsl:param name="country"></xsl:param>
	
	<!-- connecting string -->
	<xsl:variable name="connector" select="', '" />
	
	<!-- Create string, depending on how many fields exist... -->
	<xsl:choose>
		<xsl:when test="($city != '') and ($state != '') and ($country != '')">
			<xsl:value-of select="concat($city, $connector, $state, $connector, $country)"/>
		</xsl:when>
		<xsl:when test="($city != '') and ($state != '')">
			<xsl:value-of select="concat($city, $connector, $state)"/>
		</xsl:when>
		<xsl:when test="($city != '') and ($country != '')">
			<xsl:value-of select="concat($city, $connector, $country)"/>
		</xsl:when>
		<xsl:when test="($state != '') and ($country != '')">
			<xsl:value-of select="concat($state, $connector, $country)"/>
		</xsl:when>
		<xsl:when test="$city != ''">
			<xsl:value-of select="$city"/>
		</xsl:when>
		<xsl:when test="$state != ''">
			<xsl:value-of select="$state"/>
		</xsl:when>
		<xsl:when test="$country != ''">
			<xsl:value-of select="$country"/>
		</xsl:when>
	</xsl:choose>
</xsl:template>


<!-- function to format the children of a b:Person element -->
<xsl:template name="format-person-list">
    <!-- The b:Person element to format. -->
    <xsl:param name="person-list"></xsl:param>
	
	<!-- only if any entries exist... -->
	<xsl:if test="count($person-list/b:Person) > 0">
		<xsl:call-template name="formatting-persons">
		  <xsl:with-param name="person-list" select="$person-list"></xsl:with-param>
		  <xsl:with-param name="current-person-pointer" select="1"></xsl:with-param>
		</xsl:call-template>
	</xsl:if>

</xsl:template>

<!--
	XSL does not support loops. The following implements 
	the formatting of persons therefore recursively.
 -->
<xsl:template name="formatting-persons">
	<xsl:param name="person-list"></xsl:param>
	<xsl:param name="current-person-pointer"></xsl:param>
	<xsl:param name="preparedOutput"></xsl:param>
	
	<xsl:choose>
      <!-- check if there are more than persons left. -->
      <xsl:when test="count($person-list/b:Person) > $current-person-pointer">
		<!-- create text for the current person -->
		<xsl:variable name="current">
			<xsl:call-template name="format-person">
				<xsl:with-param name="person" select="$person-list/b:Person[$current-person-pointer]"></xsl:with-param>
			</xsl:call-template>
			<!-- add the connect string -->
			<xsl:text> and </xsl:text>
        </xsl:variable>
		
		<!-- re-call function with increasing counting variable by 1 -->
		<xsl:call-template name="formatting-persons">
		  <xsl:with-param name="person-list" select="$person-list"></xsl:with-param>
		  <xsl:with-param name="current-person-pointer" select="$current-person-pointer + 1"></xsl:with-param>
		  <xsl:with-param name="preparedOutput" select="concat($preparedOutput, $current)"></xsl:with-param>
		</xsl:call-template>
	  </xsl:when>
	  
      <!-- processing is finished, add final person and print. -->
      <xsl:otherwise> 
		<xsl:variable name="current">
			<!-- create text for the current person -->
			<xsl:call-template name="format-person">
				<xsl:with-param name="person" select="$person-list/b:Person[$current-person-pointer]"></xsl:with-param>
			</xsl:call-template>
        </xsl:variable>
		
		<!-- the end: print result -->
		<xsl:value-of select="concat($preparedOutput, $current)"/>
	  </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!-- function to format a b:Person element -->
<xsl:template name="format-person">
    <!-- The b:Person element to format. -->
    <xsl:param name="person"></xsl:param>
	
	<xsl:value-of select="$person/b:Last"/>
	<xsl:text>, </xsl:text> 
	<xsl:value-of select="$person/b:First"/>
	<xsl:text> </xsl:text> 
	<xsl:value-of select="$person/b:Middle"/>
</xsl:template>

<!-- tex only accepts specificly escaped characters... -->
<xsl:template name="encode-characters">
    <!-- The b:Person element to format. -->
    <xsl:param name="text"></xsl:param>
	
	
</xsl:template>

<xsl:template match="text()" /> 
</xsl:stylesheet>